import {defineRelation, Joined, SystemBaseClass} from './lib';

export class A extends SystemBaseClass {
    prop1 = true;
}

export class B extends SystemBaseClass {
    prop2 = true;
}

export class C extends SystemBaseClass {
    prop3 = true;
}

export class D extends SystemBaseClass {
    prop4 = true;
}

export const [bHasA, aHasMultipeB] = defineRelation('manyToOne', B, 'a', A, 'bs');
export type BhasA<T extends SystemBaseClass = B> = Joined<T, typeof bHasA>
export type AhasMultipleB<T extends SystemBaseClass = A> = Joined<T, typeof aHasMultipeB>

export const bHasC = defineRelation('manyToOne', B, 'c', C, undefined);
export type BhasC<T extends SystemBaseClass = B> = Joined<T, typeof bHasC>

export const bHasD = defineRelation('manyToOne', B, 'd', D, undefined);
export type BhasD<T extends SystemBaseClass = B> = Joined<T, typeof bHasD>
