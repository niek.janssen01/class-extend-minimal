"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.test12 = exports.BaseClass = void 0;
// A base class which all used classes should extend
var BaseClass = /** @class */ (function () {
    function BaseClass() {
        this.base = true;
    }
    return BaseClass;
}());
exports.BaseClass = BaseClass;
// Declare three classes for testing purposes (properties added to avoid duct typing unification)
var A = /** @class */ (function (_super) {
    __extends(A, _super);
    function A() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.a = true;
        return _this;
    }
    return A;
}(BaseClass));
var B = /** @class */ (function (_super) {
    __extends(B, _super);
    function B() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.b = true;
        return _this;
    }
    return B;
}(BaseClass));
var C = /** @class */ (function (_super) {
    __extends(C, _super);
    function C() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.c = true;
        return _this;
    }
    return C;
}(BaseClass));
function test12() {
    return undefined;
}
exports.test12 = test12;
