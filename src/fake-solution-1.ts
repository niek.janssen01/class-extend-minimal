// A base class which all used classes should extend
export class BaseClass {
    base = true;
}

// Extend a class type with a certain property, so that `Class` has a property `Class[PropertyName]: PropertyType`
export type Extend<Class extends BaseClass, PropertyName extends string, PropertyType extends BaseClass | BaseClass[]> =
    { [key in PropertyName]: PropertyType } & Class

// Modify a class type (RootClass), so that all (nested) occurrences of `PropertyBaseClass` are replaced with the extended version as defined above
export type Modify<RootClass, PropertyBase extends BaseClass,
        PropertyName extends string, PropertyType extends BaseClass | BaseClass[]> =
    // If the current RootClass extends the type of which the property should be added, add the type
    RootClass extends PropertyBase ? Extend<RootClass, PropertyName, PropertyType> :
    // Else if the current rootClass extends BaseClass (the abstract class all classes
    // in the system should inherit from), recursively map the child class types
    RootClass extends BaseClass ? {
        [K in keyof RootClass]:
        Modify<RootClass[K], BaseClass, PropertyName, PropertyType>
        // New: the `& RootClass` below
    } & RootClass :
    RootClass

// Declare three classes for testing purposes (properties added to avoid duct typing unification)
class A extends BaseClass {
    a = true;
}

class B extends BaseClass {
    b = true;

    aMethod(callback: (b: this) => void) {
        callback(this);
    }
}

class C extends BaseClass {
    c = true;
}

export type ExtA = Modify<Modify<Modify<A, A, 'b', B>, B, 'c1', C>, B, 'c2', C>

// Note: normally the casting to ExtA would be done by functions
// that actually add the properties `b` and `c` to it, like the
// `test12` function in the other examples
const a = new A() as ExtA;

a.b.aMethod(b => {
    console.log(b.c1);
    console.log(b.c2);
});