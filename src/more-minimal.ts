// A base class which all used classes should extend
export class BaseClass {
    base = true
}

// Extend a class type with a certain property, so that `Class` has a property `Class[PropertyName]: PropertyType`
export type Extend<Class extends BaseClass, PropertyName extends string, PropertyType extends BaseClass | BaseClass[]> =
        {[key in PropertyName]: PropertyType} & Class

// Modify a class type (RootClass), so that all (nested) occurrences of `PropertyBaseClass` are replaced with the extended version as defined above
export type Modify<RootClass, PropertyBase extends BaseClass,
        PropertyName extends string, PropertyType extends BaseClass|BaseClass[]> =
    // If the current RootClass extends the type of which the property should be added, add the type
    RootClass extends PropertyBase ? Extend<RootClass, PropertyName, PropertyType> :
    // Else if the current rootClass extends BaseClass (the abstract class all classes
    // in the system should inherit from), recursively map the child class types
        RootClass extends BaseClass ? {
            [K in keyof RootClass]:
                Modify<RootClass[K], BaseClass, PropertyName, PropertyType>
        } :
    RootClass

// Interface used in test8. The important part here is that Extended extends Base
export interface Test12<Base extends BaseClass, Extended extends Base> {
    x: Extended
}

export function test12<
        Base extends BaseClass,
        RelationBase extends BaseClass,
        PropertyName extends string,
        RelationClass extends BaseClass> (
): Test12<Base, Modify<Base, RelationBase, PropertyName, RelationClass>> {
    return undefined as any
}
