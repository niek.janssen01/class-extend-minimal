import {ClassOf, Definitely, } from './util';

export class SystemBaseClass {
    id = 0;
}

/**
 * Type of the join
 *
 * oneToOne is a simple oneToOne join, with the join column on the self side
 * oneToOneReversed is a simple oneToOne join, with the join column on the other side
 * manyToOne is a simple manyToOne relation, with the join column on the self side
 * oneToMany is a simple oneToMany relation, with the join column on the other side
 */
export type JoinType = 'oneToOne' | 'oneToOneReversed' | 'oneToMany' | 'manyToOne'

/**
 * Subset of [[JoinType]], which only allows for simple joins, with the join column in the base class
 */
export type BaseJoinType = 'oneToOne' | 'manyToOne'

/**
 * From any join relation type, get the reverse relation type
 *
 * @template Type The base join type
 * @returns The reverse join type of the base join type
 */
export type JoinTypeReverse<Type extends JoinType> =
        Type extends 'oneToOne' ? 'oneToOneReversed' :
        Type extends 'oneToOneReversed' ? 'oneToOne' :
        Type extends 'oneToMany' ? 'manyToOne' :
        'oneToMany'

/**
 * From any join relation type descriptor, get the reverse relation type descriptor
 */
export const joinTypeReverses: { [key in JoinType]: JoinTypeReverse<key> } = {
    oneToOne        : 'oneToOneReversed',
    oneToOneReversed: 'oneToOne',
    oneToMany       : 'manyToOne',
    manyToOne       : 'oneToMany',
};

/**
 * Resolves to the type of the relation property. It is defined as an array of entities on a oneToMany relation, and a
 * single entity otherwise.
 *
 * @template T The base type extending a SystemBaseClass.
 * @template Type The join type
 * @returns The type of the joined property: T[] for oneToMany relations, T otherwise
 */
export type JoinedAs<T extends SystemBaseClass, Type extends JoinType> = Type extends 'oneToMany' ? T[] : T

/**
 * Adds a joined property to a certain base class, using the [[JoinType]], base class, property name and relation class
 * type.
 *
 * @template Type The relation type
 * @template PropertyName The name of the relation property
 * @template RelationClass The entity class type of the relation
 * @returns An object type that has the relation as a property, using [[JoinedAs]] as the exact relation type
 */
export type PropertyJoined<Type extends JoinType, BaseClass extends SystemBaseClass, PropertyName extends string, RelationClass extends SystemBaseClass> =
        { [key in PropertyName]: JoinedAs<RelationClass, Type> } & BaseClass
/**
 * Deep join a relation onto any entity in a join tree having a specific entity type.
 */
/**
 * Helper type to recursively replace all properties with type `TReplace` with properties of type `TWith` recursively in
 * a nested object type.
 */
export type DeepJoined<Type extends JoinType, CurrentType, BaseClass extends SystemBaseClass, PropertyName extends string, RelationClass extends SystemBaseClass> =
        CurrentType extends BaseClass ? PropertyJoined<Type, CurrentType, PropertyName, RelationClass> :
        CurrentType extends SystemBaseClass ?
        {
            [K in keyof CurrentType]:
            CurrentType[K] extends (infer SetClass)[] ?
            DeepJoined<Type, SetClass, BaseClass, PropertyName, RelationClass>[] :
            DeepJoined<Type, CurrentType[K], BaseClass, PropertyName, RelationClass>
        } & CurrentType:
        CurrentType
/**
 * Wrapper for [[DeepJoined]]
 */
export type Joined<RootClass extends SystemBaseClass, Descriptor extends RelationDescriptor> =
        Descriptor extends RelationDescriptor<infer Type, infer BaseClass, infer PropertyName, infer RelationClass, any> ?
        DeepJoined<Type, RootClass, BaseClass, PropertyName, RelationClass> : never

/**
 * A strongly typed relation descriptor. Describes the relation, as well as the types of all the properties and relation
 * classes.
 */
export interface RelationDescriptor<Type extends JoinType = JoinType, BaseClass extends SystemBaseClass = SystemBaseClass, PropertyName extends string = string, RelationClass extends SystemBaseClass = SystemBaseClass, ReverseProperty extends string | undefined = string | undefined> {
    relationType: Type;
    baseClass: ClassOf<BaseClass>;
    propertyName: PropertyName;
    relationClass: ClassOf<RelationClass>;
    reverseProperty: ReverseProperty;
}

export function createRelationDescriptor<Type extends JoinType, BaseClass extends SystemBaseClass, PropertyName extends string, RelationClass extends SystemBaseClass, ReverseProperty extends string | undefined>(
        relationType: Type,
        baseClass: ClassOf<BaseClass>, propertyName: PropertyName,
        relationClass: ClassOf<RelationClass>, reverseProperty: ReverseProperty,
): RelationDescriptor<Type, BaseClass, PropertyName, RelationClass, ReverseProperty> {
    return {relationType, baseClass, propertyName, relationClass, reverseProperty};
}

/**
 * Create a reverse relation descriptor from an existing relation descriptor
 *
 * @param relationType
 * @param baseClass
 * @param propertyName
 * @param relationClass
 * @param reverseProperty
 */
export function createReverseRelationDescriptor<Type extends JoinType, BaseClass extends SystemBaseClass, PropertyName extends string, RelationClass extends SystemBaseClass, ReverseProperty extends string>
({relationType, baseClass, propertyName, relationClass, reverseProperty}:
        RelationDescriptor<Type, BaseClass, PropertyName, RelationClass, ReverseProperty>):
        RelationDescriptor<JoinTypeReverse<Type>, RelationClass, ReverseProperty, BaseClass, PropertyName> {
    return createRelationDescriptor<JoinTypeReverse<Type>, RelationClass, ReverseProperty, BaseClass, PropertyName>(joinTypeReverses[relationType], relationClass, reverseProperty, baseClass, propertyName);
}

/**
 * Create a relation. If the reverseProperty is set, also create the reverse relation. The created relations are
 * automatically registered with typeorm, and the relations are created in a way that joining can be done type safe.
 *
 * @param type
 * @param baseClass
 * @param propertyName
 * @param relationClass
 * @param reverseProperty
 * @param options One or more Kws Column options, as defined in [[KwsColumnOptions]] and other subsystems
 * @returns A tuple with both the base and reverse relations. Note that the reverse relation only exists when the
 *          `reverseProperty` is defined.
 */
export function defineRelation<Type extends BaseJoinType,
        BaseClass extends SystemBaseClass,
        PropertyName extends string,
        RelationClass extends SystemBaseClass,
        ReverseProperty extends string>(
        type: Type,
        baseClass: ClassOf<BaseClass>,
        propertyName: PropertyName,
        relationClass: ClassOf<RelationClass>,
        reverseProperty: ReverseProperty,
): [
    RelationDescriptor<Type, BaseClass, PropertyName, RelationClass, ReverseProperty>,
    RelationDescriptor<JoinTypeReverse<Type>, RelationClass, ReverseProperty, BaseClass, PropertyName>
]

/**
 * Create a relation. If the reverseProperty is set, also create the reverse relation. The created relations are
 * automatically registered with typeorm, and the relations are created in a way that joining can be done type safe.
 *
 * @param type
 * @param baseClass
 * @param propertyName
 * @param relationClass
 * @param _reverseProperty Should be left undefined, used to align the parameters with the version including a `reverseProperty`.
 * @param options One or more Kws Column options, as defined in [[KwsColumnOptions]] and other subsystems
 * @returns A tuple with both the base and reverse relations. Note that the reverse relation only exists when the
 *          `reverseProperty` is defined.
 */
export function defineRelation<Type extends BaseJoinType,
        BaseClass extends SystemBaseClass,
        PropertyName extends string,
        RelationClass extends SystemBaseClass>(
        type: Type,
        baseClass: ClassOf<BaseClass>,
        propertyName: PropertyName,
        relationClass: ClassOf<RelationClass>,
        _reverseProperty: undefined,
): RelationDescriptor<Type, BaseClass, PropertyName, RelationClass>


export function defineRelation<Type extends BaseJoinType,
        BaseClass extends SystemBaseClass,
        PropertyName extends string,
        RelationClass extends SystemBaseClass,
        ReverseProperty extends string | undefined>(
        type: Type,
        baseClass: ClassOf<BaseClass>,
        propertyName: PropertyName,
        relationClass: ClassOf<RelationClass>,
        reverseProperty: ReverseProperty,
): [
       RelationDescriptor<Type, BaseClass, PropertyName, RelationClass, ReverseProperty>,
       RelationDescriptor<JoinTypeReverse<Type>, RelationClass, ReverseProperty & string, BaseClass, PropertyName>
   ] | RelationDescriptor<Type, BaseClass, PropertyName, RelationClass> {
    const mainRelationDescriptor = createRelationDescriptor<Type, BaseClass, PropertyName, RelationClass, ReverseProperty>(type, baseClass, propertyName, relationClass, reverseProperty);

    if (mainRelationDescriptor.reverseProperty === undefined)
        return mainRelationDescriptor;

    const reverseRelationDescriptor = createReverseRelationDescriptor(mainRelationDescriptor as RelationDescriptor<Type, BaseClass, PropertyName, RelationClass, Definitely<ReverseProperty>>);
    return [mainRelationDescriptor, reverseRelationDescriptor];
}
