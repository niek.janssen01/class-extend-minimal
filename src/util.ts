/**
 * Type of an any object
 */
export type Obj = { [key: string]: any }

/**
 * The type of a Class T.
 *
 * @template T The class of which the ClassOf<T> is the type
 */
export type ClassOf<T> = new() => T
/**
 * Remove undefined from a type.
 *
 * @template S The type that should not be undefined
 * @template T The type S or undefined
 */
export type Definitely<T> = NonNullable<T>
