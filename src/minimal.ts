// A base class which all used classes should extend
export class BaseClass {
    base = true
}

// Extend a class type with a certain property, so that `Class` has a property `Class[PropertyName]: PropertyType`
export type Extend<Class extends BaseClass, PropertyName extends string, PropertyType extends BaseClass | BaseClass[]> =
        {[key in PropertyName]: PropertyType} & Class

// Modify a class type (RootClass), so that all (nested) occurrences of `PropertyBaseClass` are replaced with the extended version as defined above
export type Modify<RootClass, PropertyBase extends BaseClass,
            PropertyName extends string, PropertyType extends BaseClass|BaseClass[]> =
        // If the current RootClass extends the type of which the property should be added, add the type
        RootClass extends PropertyBase ? Extend<RootClass, PropertyName, PropertyType> :
        // Else if the current rootClass extends BaseClass (the abstract class all classes
        // in the system should inherit from), recursively map the child class types
        RootClass extends BaseClass ? {
                  [K in keyof RootClass]:
                        Modify<RootClass[K], BaseClass, PropertyName, PropertyType>
              } :
        RootClass

// Declare three classes for testing purposes (properties added to avoid duct typing unification)
class A extends BaseClass{
    a= true
}

class B extends BaseClass{
    b= true
}

class C extends BaseClass{
    c= true
}

// This test succeeds:
export type Test10<Base extends BaseClass, Extended extends Base> =
        Modify<Extended, B, 'c', C> extends Base ?true : false
export type Test11 = Test10<A, Extend<A, 'b', B>>
// According to the typescript compiler, Type11 = `true`

// Interface used in test8. The important part here is that Extended extends Base
export interface Test8<Base extends BaseClass, Extended extends Base> {
    x: Extended
}

export function test8<
        Base extends BaseClass,
        Extended extends Base,
        RelationBase extends BaseClass,
        PropertyName extends string,
        RelationClass extends BaseClass> (
        _: Test8<Base, Extended>
): Test8<Base, Modify<Extended, RelationBase, PropertyName, RelationClass>> {
    return undefined as any
}
