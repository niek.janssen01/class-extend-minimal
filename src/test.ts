import {A, AhasMultipleB, B, bHasC, BhasC, BhasD, C}                       from './classes';
import {DeepJoined, Joined, JoinType, RelationDescriptor, SystemBaseClass} from './lib';

export function test1(a: BhasD<BhasC<AhasMultipleB>>) {
    a.bs
            .filter(b => !b.c.prop3)
            .filter(b => !b.d.prop4);
}

export function test2(a: BhasC<BhasD<AhasMultipleB>>) {
    a.bs
            .filter(b => !b.c.prop3)
            .filter(b => !b.d.prop4);
}

export type ArrayItem<T extends any[]> = T extends (infer SubType)[] ? SubType : never

export type Test3 = BhasC<BhasD<AhasMultipleB>> extends A ? true : false
export type Test4 = ArrayItem<BhasC<BhasD<AhasMultipleB>>['bs']> extends B ? true : false
export type Test5 = ArrayItem<BhasC<BhasD<AhasMultipleB>>['bs']>['c'] extends C ? true : false

export type Test6<Base extends SystemBaseClass, Ext extends Base> =
    Joined< Ext, typeof bHasC> extends Base ? true : false

export type Test7 = Test6<A, AhasMultipleB>

export type Test10<Base extends SystemBaseClass, Ext extends Base> =
    DeepJoined<'oneToMany', Ext, B, 'c', C> extends Base ? true: false

export type Test11 = Test10<A, AhasMultipleB>

export interface Test8<Base extends SystemBaseClass, Extended extends Base> {
    x: Extended
}

export function test8<
        Base extends SystemBaseClass,
        Extended extends Base,
        RelationBase extends SystemBaseClass,
        PropertyName extends string,
        RelationClass extends SystemBaseClass> (
                _: Test8<Base, Extended>
        ) : Test8<Base, DeepJoined<'manyToOne', Extended, RelationBase, PropertyName, RelationClass>>{
    return undefined as any
}

export function test9<
        Base extends SystemBaseClass,
        Extended extends Base,
        Type extends JoinType,
        RelationBase extends SystemBaseClass,
        PropertyName extends string,
        RelationClass extends SystemBaseClass,
        Relation extends RelationDescriptor<Type, RelationBase, PropertyName, RelationClass>> (
        _: Test8<Base, Extended>
) : Test8<Base, Joined<Extended, Relation>>{
    return undefined as any
}

export function test12<
        Base extends SystemBaseClass,
        RelationBase extends SystemBaseClass,
        PropertyName extends string,
        RelationClass extends SystemBaseClass> (
): Test8<Base, DeepJoined<Base, RelationBase, PropertyName, RelationClass>> {
    return undefined as any
}
