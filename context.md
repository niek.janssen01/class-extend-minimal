## Context

I'm trying to create a layer on top of an existing ORM. The part I'm having trouble with is typesafe joins. My goal is
to have an entity class _not_ have any properties that aren't joined. The logic of this is completely fine, but this
also means that the bare Entity classes have no information whatsoever about relation columns. This information has to
be added to the base type with the type system as soon as the join is done. This question is about the creation of _
types_ that have their _relation properties_ added to the _base class_.

For example:

```typescript
export class A extends SystemBaseClass {
    prop1: number;
}

export class B extends SystemBaseClass {
    prop2: number;
}
```

_Example 1_

A many to one relation between A and B exists, with the join column on the **B** side (The B side is for consistency
with other examples). If everything was joined, the relations between A and B should be:

```typescript
export class A extends SystemBaseClass {
    prop1: number;
    bs: B[]
}

export class B extends SystemBaseClass {
    prop2: number;
    a: A
}
```

_Example 2_

## Relevant implementation details

### Types of relations

We first want to setup a way to create the types. This is done via the function `defineRelation`. The
function `defineRelation` returns an object called a Relation Descriptor. This Relation descriptor has templated types
for each field, so the information about property names and entity types is available during runtime as well as during
compile time:

```typescript
// The relation descriptor interface, storing information about properties and entity types during compile time and runtime
export interface RelationDescriptor<Type extends JoinType = JoinType,
        BaseClass extends SystemBaseClass = SystemBaseClass,
        PropertyName extends string = string,
        RelationClass extends SystemBaseClass = SystemBaseClass,
        ReverseProperty extends string | undefined = string | undefined> {
    relationType: Type;
    baseClass: ClassOf<BaseClass>;
    propertyName: PropertyName;
    relationClass: ClassOf<RelationClass>;
    reverseProperty: ReverseProperty;
}


// The definition of the relation between entity types A and B
export const [bHasA, aHasMultipeB] = defineRelation('manyToOne', B, 'a', A, 'bs');
export type BhasA<T extends SystemBaseClass = B> = Joined<T, typeof bHasA>
export type AhasMultipleB<T extends SystemBaseClass = A> = Joined<T, typeof aHasMultipeB>
```

_Example 3_

As can be seen in example 3, the types `BhasA` and `AhasMultipleB` can be used to extend a type B to have the property '
a' and type A to have property 'bs'.

### Nested relations

Let us look at the example below:

```
A <- B -> C
       -> D
```

_Example 4_

Here, we have entity classes A, B, C and D. B has a many to one relation to A, C and D. All join columns are inside of
B.

From this example, we want to take a class of type A, and join all relations to it. Using the methods to define
relations described in _example 3_, we should be able to construct this type:

```typescript
// Read from inside (right) to outside (left), as is common with nestings
export type AandAllJoined = BhasD<BhasC<AhasMultipleB>>
```

_Example 5_

In this example we have `AhasMultipleB`, which adds the `bs` property to entity type A. However, when we get to `BhasC`,
something interesting happens. `BhasC` now digs into the type of `AhasMultipleB`, and replaces the type `B` of `A.bs`
with a type `B` with an added property `c:C`.

### `Joined`, the relation extension type

The magic to make this all possible, happens inside the `Joined` type visible at the bottom of example 3. `Joined` takes
an entity class type with any amount of relations already joined, and a relation descriptor. Let's unpack the arguments
to:

Given entity class type T
Relation descriptor base class B
Relation descriptor property name P
Relation descriptor relation class R

`Joined` now should return a modified version of `T`, where all occurrences of `B` (also deeper nested occurrences) have
been replaced by a version of `B` that contains a property `P` of type `R`.

The implementation is given in example 6. I already marked two problematic area's in the comments with `/*!!!*/

```typescript
/**
 * Resolves to the type of the relation property. It is defined as an array of entities on a oneToMany relation, and a
 * single entity otherwise.
 *
 * @template T The base type extending a SystemBaseClass.
 * @template Type The join type
 * @returns The type of the joined property: T[] for oneToMany relations, T otherwise
 */
export type JoinedAs<T extends SystemBaseClass, Type extends JoinType> = Type extends 'oneToMany' ? T[] : T

/**
 * Adds a joined property to a certain base class, using the [[JoinType]], base class, property name and relation class
 * type.
 *
 * @template Type The relation type
 * @template PropertyName The name of the relation property
 * @template RelationClass The entity class type of the relation
 * @returns An object type that has the relation as a property, using [[JoinedAs]] as the exact relation type
 */
export type PropertyJoined<Type extends JoinType, PropertyName extends string, RelationClass extends SystemBaseClass> =
        { [key in PropertyName]: JoinedAs<RelationClass, Type> }

/**
 * Deep join a relation onto any entity in a join tree having a specific entity type.
 *
 * @template Type The type of the relation
 * @template RootClass The deep nested entity class type on which the join needs to be performed
 * @template BaseClass The base entity class type of the relation
 * @template PropertyName The type of the property name, as a string literal type
 * @template RelationClass The entity class type of the joined entity
 * @returns The RootClass, on which all (nested) occurrences of the BaseClass have the relation described by the
 *          BaseClass, PropertyName and RelationClass joined as an extra property
 */
export type DeepJoined<Type extends JoinType, RootClass extends SystemBaseClass, BaseClass extends SystemBaseClass, PropertyName extends string, RelationClass extends SystemBaseClass> =
        _DeepJoined<RootClass, BaseClass, PropertyJoined<Type, PropertyName, RelationClass>>

/**
 * Helper type of DeepJoined, on which `Type`, `BaseClass`, `PropertyName` and `RelationClass` are aggregated as a
 * JoinedProperty
 */
type _DeepJoined<RootClass, BaseClass, JoinedProperty> =
        RootClass extends BaseClass ? /*!!!*/JoinedProperty & RootClass /*!!!*/ :
        RootClass extends Obj ?
        {
            [K in keyof RootClass]:
            RootClass[K] extends (infer SetClass)[] ?
            _DeepJoined<SetClass, BaseClass, JoinedProperty>[] :
            _DeepJoined<RootClass[K], BaseClass, JoinedProperty>
            /*!!!*/
        } & RootClass /*!!!*/ :
        RootClass

/**
 * Wrapper for [[DeepJoined]]
 */
export type Joined<RootClass extends SystemBaseClass, Descriptor extends RelationDescriptor> =
        Descriptor extends RelationDescriptor<infer Type, infer BaseClass, infer PropertyName, infer RelationClass, any> ?
        DeepJoined<Type, RootClass, BaseClass, PropertyName, RelationClass> : never
```