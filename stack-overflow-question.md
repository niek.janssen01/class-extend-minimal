# Recursively expanding class types with extra properties in a correct way

I've got a problem I've been breaking my neck over for more then a week now, and I cannot seem to find a correct
solution. I really hope there is a level 99 typescript type wizard around here that can cook up a possible solution.

**This question is about compile time types, not about the runtime implementation!**

I'm trying to create type modifiers for class types to add properties or change the type of a property. However, I want 
to do this in a way that does not lose the constructor. 

At the moment, I use the following type constructs to accomplish adding/modifying a type (simplified):

```typescript
// A base class which all used classes should extend
export class BaseClass {
    base = true
}

// Extend a class type with a certain property, 
// so that `Class` has a property `Class[PropertyName]: PropertyType`
export type Extend<Class extends BaseClass, PropertyName extends string, PropertyType extends BaseClass | BaseClass[]> =
        {[key in PropertyName]: PropertyType} & Class

// Modify a class type (RootClass), so that all (nested) occurrences of `PropertyBaseClass` 
// are replaced with the extended version as defined above
export type Modify<RootClass, PropertyBase extends BaseClass,
        PropertyName extends string, PropertyType extends BaseClass|BaseClass[]> =
    // If the current RootClass extends the type of which the property should be added, add the type
    RootClass extends PropertyBase ? Extend<RootClass, PropertyName, PropertyType> :
    // Else if the current rootClass extends BaseClass (the abstract class all classes
    // in the system should inherit from), recursively map the child class types
        RootClass extends BaseClass ? {
            [K in keyof RootClass]:
                Modify<RootClass[K], BaseClass, PropertyName, PropertyType>
        } :
    RootClass
```

Now, the problem I have with this construct, is the following behaviour I do not fully understand:

```typescript
// Interface used in test8. The important part here is that Extended extends Base
export interface Test12<Base extends BaseClass, Extended extends Base> {
    x: Extended
}

export function test12<
        Base extends BaseClass,
        RelationBase extends BaseClass,
        PropertyName extends string,
        RelationClass extends BaseClass> (
): Test12<Base, Modify<Base, RelationBase, PropertyName, RelationClass>> {
    return undefined as any
}
```

In this example, function `test12` throws a type error. Because it is long, I only included the first and last line:

```
TS2344: 
Type 'Modify<Base, RelationBase, PropertyName, RelationClass>' 
    does not satisfy the constraint 'Base'. 
Modify<Base, RelationBase, PropertyName, RelationClass>' 
    is assignable to the constraint of type 'Base', 
    but 'Base' could be instantiated with a different subtype of constraint 'BaseClass'.
...
'Extend<RelationBase & BaseClass, PropertyName, RelationClass>' 
    is assignable to the constraint of type 'Base', 
    but 'Base' could be instantiated with a different subtype of constraint 'BaseClass'.
```

Note: A repository created for testing purposes can be found below. Run `tsc src/more-minimal.ts` to run the code examples above.

[https://gitlab.com/niek.janssen01/class-extend-minimal](https://gitlab.com/niek.janssen01/class-extend-minimal)

My guess is that test10/test11 work, because it is only really instantiated with a concrete example that seems to be 
allowed, but test8 for some reason is not. My concrete questions are:

- Why exactly am I getting this error? 
- Is there a way to implement this in a neat way? Maybe constrain the types of function `test8` a bit more so that `Modify<...> does actually extend `Base`?

## Things I've tried
### Force the type of `Modify` to be `& RootClass`:

```typescript
export type Modify<RootClass, PropertyBase extends BaseClass,
        PropertyName extends string, PropertyType extends BaseClass|BaseClass[]> =
    // If the current RootClass extends the type of which the property should be added, add the type
    RootClass extends PropertyBase ? Extend<RootClass, PropertyName, PropertyType> :
    // Else if the current rootClass extends BaseClass (the abstract class all classes
    // in the system should inherit from), recursively map the child class types
        RootClass extends BaseClass ? {
            [K in keyof RootClass]:
                Modify<RootClass[K], BaseClass, PropertyName, PropertyType>
                // New: the `& RootClass` below
        } & RootClass:
    RootClass
```

- Pro: The error is gone
- Con: It seems a bit hacky, as it is now both the modified type and the unmodified type at once
- Con: It seems that in some cases, function calls seem to break. However, I haven't been able to reproduce this issue 
  in this simplified version of the problem/solution yet. 

### Work around this issue by removing the `Extended extends Base` dependency in `Test12`
This is simply not possible because this dependency is needed elsewhere in the code
